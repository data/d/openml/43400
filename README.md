# OpenML dataset: COVID-19-community-mobility-reports

https://www.openml.org/d/43400

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The pandemic context brings new challenges to cities. This fantastic resource was created by Google with aggregated, anonymized sets of data from users who have turned on the Location History setting on Android. It captures the changes in mobility between baseline values (median value from the 5week period Jan 3  Feb 6, 2020) at weekly intervals data from February to October 2020. Data include changes in mobility related to workplace, residential, transit hubs, parks, retail and grocery. It covers 593 cities in 43 countries. 
See https://ourworldindata.org/covid-mobility-trends

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43400) of an [OpenML dataset](https://www.openml.org/d/43400). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43400/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43400/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43400/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

